package com.dream.constant;

public class RedisConstant {
    /**
     * 一分钟
     **/
    public static final int ONE_MINUTE = 60;

    /**
     * 五分钟
     **/
    public static final int FIVE_MINUTES = 60 * 5;


    /**
     * 十分钟
     **/
    public static final int TEN_MINUTES = 60 * 10;

    /**
     * 三十分钟
     **/
    public static final int HALF_HOUR = 60 * 30;

    /**
     * 一小时
     **/
    public static final int ONE_HOUR = 60 * 60;

    /**
     * 12小时
     **/
    public static final int TWELVE_HOUR = 60 * 60 * 12;

    /**
     * 一天
     **/
    public static final int ONE_DAY = 60 * 60 * 24;

    /**永久**/
    public static final int PERMANENT = 0;
}

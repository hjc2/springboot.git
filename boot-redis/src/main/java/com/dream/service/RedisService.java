package com.dream.service;

import com.dream.entity.BookEntity;
import org.springframework.web.bind.annotation.RequestMapping;

public interface RedisService {
    void add(BookEntity bookEntity);
}

package com.dream.controller;

import com.dream.entity.BookEntity;
import com.dream.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RedisController {
    @Autowired
    private RedisService redisService;

    @RequestMapping(value = "/add")
    public void add(BookEntity bookEntity) {
        redisService.add(bookEntity);
    }
}

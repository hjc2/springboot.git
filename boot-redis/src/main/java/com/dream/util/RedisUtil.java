package com.dream.util;

import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
public class RedisUtil {
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 功能描述: <br>
     * 〈设置redis的值〉
     *
     * @Param: [key, object]
     * @Return: boolean
     * @Author: huajiancheng
     * @Date: 2020/9/12
     */
    public boolean set(String key, Object object) {
        try {
            redisTemplate.opsForValue().set(key, JSON.toJSONString(object));
            return true;
        } catch (Exception e) {
            System.out.println("redis设置值发生异常--->" + e);
        }
        return false;
    }

    /**
     * 功能描述: <br>
     * 〈设置redis的值〉
     *
     * @Param: [key, object,seconds]
     * @Return: boolean
     * @Author: huajiancheng
     * @Date: 2020/9/12
     */
    public boolean set(String key, Object object, int seconds) {
        try {
            redisTemplate.opsForValue().set(key, JSON.toJSONString(object));
            expire(key, seconds);
            return true;
        } catch (Exception e) {
            System.out.println("redis设置值发生异常--->" + e);
        }
        return false;
    }

    /**
     * 功能描述: <br>
     * 〈设置redis超时时间〉
     *
     * @Param: [key, seconds]
     * @Return: boolean
     * @Author: huajiancheng
     * @Date: 2020/9/12
     */
    public boolean expire(String key, int seconds) {
        try {
            return redisTemplate.expire(key, seconds, TimeUnit.SECONDS);
        } catch (Exception e) {
            System.out.println("redis设置超时时间发生异常--->" + e);
        }
        return false;
    }

    /**
     * 功能描述: <br>
     * 〈判断key存不存在〉
     *
     * @Param: [key]
     * @Return: boolean
     * @Author: huajiancheng
     * @Date: 2020/9/12
     */
    public boolean hasKey(String key) {
        try {
            return redisTemplate.hasKey(key);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 功能描述: <br>
     * 〈删除缓存〉
     *
     * @Param: [key]
     * @Return: boolean
     * @Author: huajiancheng
     * @Date: 2020/9/12
     */
    public boolean remove(String key) {
        try {
            return redisTemplate.delete(key);
        } catch (Exception e) {
            System.out.println("删除缓存时发生异常---->" + e);
        }
        return false;
    }

    /**
     * 功能描述: <br>
     * 〈获取key的value〉
     *
     * @Param: [key]
     * @Return: java.lang.String
     * @Author: huajiancheng
     * @Date: 2020/9/12
     */
    public Object get(String key) {
        try {
            return redisTemplate.opsForValue().get(key);
        } catch (Exception e) {
            System.out.println("获取redis值发生异常--->" + e);
        }
        return null;
    }

    /**
     * 根据key 获取过期时间
     *
     * @param key 键 不能为null
     * @return 时间(秒) 返回0代表为永久有效
     */
    public long getExpire(String key) {
        return redisTemplate.getExpire(key, TimeUnit.SECONDS);
    }

    /**
     * 功能描述: <br>
     * 〈设置DB分区〉
     *
     * @Param: [index]
     * @Return: void
     * @Author: huajiancheng
     * @Date: 2020/9/12
     */
    public void setDataBase(int index) {
        LettuceConnectionFactory connectionFactory = (LettuceConnectionFactory) redisTemplate.getConnectionFactory();
        if (connectionFactory != null && index != connectionFactory.getDatabase()) {
            connectionFactory.setDatabase(index);
            this.redisTemplate.setConnectionFactory(connectionFactory);
            connectionFactory.resetConnection();
        }
    }

    /**
     * 获取hashKey对应的所有键值
     *
     * @param key 键
     * @return 对应的多个键值
     */
    public Map<Object, Object> hmget(String key) {
        return redisTemplate.opsForHash().entries(key);
    }


    /**
     * HashSet
     *
     * @param key 键
     * @param map 对应多个键值
     * @return true 成功 false 失败
     */
    public boolean hmset(String key, Map<String, Object> map) {
        try {
            redisTemplate.opsForHash().putAll(key, map);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}

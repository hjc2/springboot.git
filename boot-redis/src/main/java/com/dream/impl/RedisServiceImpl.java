package com.dream.impl;

import com.dream.constant.RedisConstant;
import com.dream.entity.BookEntity;
import com.dream.service.RedisService;
import com.dream.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class RedisServiceImpl implements RedisService {

    @Autowired
    RedisUtil redisUtil;

    @Override
    public void add(BookEntity bookEntity) {
        // 增加 不写时间默认-1代表永久有效
        redisUtil.set("3", bookEntity);
        redisUtil.set("4", bookEntity, RedisConstant.ONE_MINUTE);
        // 根据key取值
        String value = String.valueOf(redisUtil.get("4"));
        System.out.println(value);
        // 根据key删除
        redisUtil.remove("4");
        long keyTime = redisUtil.getExpire("3");
        System.out.println(keyTime);

        Map<String, Object> map = BeanMap.create(bookEntity);
        redisUtil.hmset("5", map);
        redisUtil.expire("5", RedisConstant.ONE_MINUTE);
        Map<Object, Object> objectMap = redisUtil.hmget("5");
        System.out.println(objectMap);
    }
}

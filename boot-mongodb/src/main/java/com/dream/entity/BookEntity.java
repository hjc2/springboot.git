package com.dream.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection="BookEntity")
public class BookEntity {
    /**
     * ID编号
     **/
    @Id
    private String id;
    /**
     * 图书名称
     **/
    @Field("bookName")
    private String bookName;
    /**
     * 图书作者
     **/
    @Field("author")
    private String author;
    /**
     * 图书价格
     **/
    @Field("bookPrice")
    private float bookPrice;
    /**
     * 图书发布时间
     **/
    @Field("publishTime")
    private String publishTime;

    public BookEntity() {
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public float getBookPrice() {
        return bookPrice;
    }

    public void setBookPrice(float bookPrice) {
        this.bookPrice = bookPrice;
    }

    public String getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(String publishTime) {
        this.publishTime = publishTime;
    }
}

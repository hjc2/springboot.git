package com.dream.service;

import com.dream.entity.BookEntity;

public interface MongoDBService {
    void add(BookEntity entity);
    void queryOne(BookEntity entity);
    void update(BookEntity entity);
    void delete(BookEntity entity);
    void queryList(BookEntity entity);
    void queryAll(BookEntity entity);
}

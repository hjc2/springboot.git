package com.dream.controller;

import com.dream.entity.BookEntity;
import com.dream.service.MongoDBService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MongoDBController {
    @Autowired
    MongoDBService mongoDBService;

    /**
     * 功能描述: <br>
     * 〈新增数据〉
     *
     * @Param: [entity]
     * @Return: void
     * @Author: huajiancheng
     * @Date: 2020/9/10
     */
    @RequestMapping(value = "/add")
    public void add(BookEntity entity) {
        mongoDBService.add(entity);
    }

    /**
     * 功能描述: <br>
     * 〈查询一条数据〉
     *
     * @Param: [entity]
     * @Return: void
     * @Author: huajiancheng
     * @Date: 2020/9/10
     */
    @RequestMapping(value = "/queryOne")
    public void queryOne(BookEntity entity) {
        mongoDBService.queryOne(entity);
    }

    /**
     * 功能描述: <br>
     * 〈查询列表数据〉
     *
     * @Param: [entity]
     * @Return: void
     * @Author: huajiancheng
     * @Date: 2020/9/10
     */
    @RequestMapping(value = "/queryList")
    public void queryList(BookEntity entity) {
        mongoDBService.queryList(entity);
    }


    @RequestMapping(value = "/queryAll")
    public void queryAll(BookEntity entity) {
        mongoDBService.queryAll(entity);
    }

    /**
     * 功能描述: <br>
     * 〈更新一条数据〉
     *
     * @Param: [entity]
     * @Return: void
     * @Author: huajiancheng
     * @Date: 2020/9/10
     */
    @RequestMapping(value = "/update")
    public void update(BookEntity entity) {
        mongoDBService.update(entity);
    }

    /**
     * 功能描述: <br>
     * 〈删除一条数据〉
     *
     * @Param: [entity]
     * @Return: void
     * @Author: huajiancheng
     * @Date: 2020/9/10
     */
    @RequestMapping(value = "/delete")
    public void delete(BookEntity entity) {
        mongoDBService.delete(entity);
    }
}

package com.dream.impl;

import com.alibaba.fastjson.JSON;
import com.dream.entity.BookEntity;
import com.dream.service.MongoDBService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.regex.Pattern;

@Service
public class MongoDBServiceImpl implements MongoDBService {

    @Autowired
    private MongoTemplate mongoTemplate;


    @Override
    public void add(BookEntity entity) {
        // 保存
        mongoTemplate.save(entity);
    }

    // 根据id查询
    @Override
    public void queryOne(BookEntity entity) {
        String id = entity.getId();
        Query query = new Query(Criteria.where("_id").is(id));
        BookEntity bookEntity = mongoTemplate.findOne(query, BookEntity.class);
        System.out.println(JSON.toJSONString(bookEntity));
        String bookName = entity.getBookName();
        Query queryByBookName = new Query(Criteria.where("bookName").is(bookName));
        bookEntity = mongoTemplate.findOne(queryByBookName, BookEntity.class);
        System.out.println(JSON.toJSONString(bookEntity));
    }

    // 更新一条数据
    @Override
    public void update(BookEntity entity) {
        String id = entity.getId();
        Criteria criteria = new Criteria();
        Query query = new Query(criteria.where("_id").is(id));
        Update update = new Update();
        float bookPrice = entity.getBookPrice();
        update.set("bookPrice", bookPrice);
        // 更新所有返回集
        mongoTemplate.updateMulti(query, update, BookEntity.class);
        // 更新返回集的第一个
//        mongoTemplate.updateFirst(query,update,BookEntity.class);
    }

    @Override
    public void delete(BookEntity entity) {
        // 移除
        mongoTemplate.remove(entity);
    }


    // 模糊查询
    @Override
    public void queryList(BookEntity entity) {
        String bookName = entity.getBookName();
        Query query = new Query();
        Criteria criteria = new Criteria();
        Pattern pattern = Pattern.compile("^.*" + bookName + ".*$", Pattern.CASE_INSENSITIVE);
        criteria.where("bookName").regex(pattern);
//        List<BookEntity> lists = mongoTemplate.findAllAndRemove(query, BookEntity.class);
        List<BookEntity> lists = mongoTemplate.find(query, BookEntity.class);
        System.out.println(JSON.toJSONString(lists));
    }

    // 查询所有
    @Override
    public void queryAll(BookEntity entity) {
        List<BookEntity> list = mongoTemplate.findAll(BookEntity.class);
        System.out.println(JSON.toJSONString(list));
    }
}

package com.dream.impl;

import com.dream.dao.UserMapper;
import com.dream.service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IndexServiceImpl implements IndexService {
    @Autowired
    UserMapper UserMapper;

    @Override
    public int getCount() {
        return UserMapper.getCount1();
    }
}

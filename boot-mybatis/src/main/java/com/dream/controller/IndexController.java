package com.dream.controller;

import com.dream.service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {
    @Autowired
    IndexService indexService;

    @RequestMapping(value = "/index")
    public String index() {
        return String.valueOf(indexService.getCount());
    }
}

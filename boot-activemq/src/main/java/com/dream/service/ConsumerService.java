package com.dream.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

@Component
public class ConsumerService {
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    // 使用JmsListener配置消费者监听的队列，其中name是接收到的消息
    @JmsListener(destination = "active.queue")
    // SendTo 会将此方法返回的数据, 写入到 OutQueue 中去.
    @SendTo(value = "Squeue")
    public String handleMessage(String name) {
        System.out.println("成功接受Name" + name);
        return "成功接受Name" + name;
    }




//    监听和接收主题消息1
    @JmsListener(destination = "active.topic")
    public void readActiveTopic1(String message) {
        System.out.println("Customer1接受到：" + message);
    }


    //    监听和接收主题消息2
    @JmsListener(destination = "active.topic")
    public void readActiveTopic2(String message) {
        System.out.println("Customer2接受到：" + message);
    }

}

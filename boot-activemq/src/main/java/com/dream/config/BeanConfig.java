package com.dream.config;

import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.jms.Queue;
import javax.jms.Topic;


@Configuration
public class BeanConfig {
    //    定义存放消息的队列 点对点模式
    @Bean
    public Queue queue() {
        return new ActiveMQQueue("active.queue");
    }

    // 发布订阅模式
    @Bean
    public Topic topic() {
        return new ActiveMQTopic("active.topic");
    }
}

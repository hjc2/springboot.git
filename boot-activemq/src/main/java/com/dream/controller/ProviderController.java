package com.dream.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.jms.Queue;
import javax.jms.Topic;

@RestController
@RequestMapping(value = "/mq")
public class ProviderController {
    //    注入存放消息的队列
    @Autowired
    private Queue queue;
    @Autowired
    private Topic topic;

    //    注入spring boot封装的工具
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    @RequestMapping(value = "/sendQueue")
    public void sendQueue(String name) {
//        方法一：添加消息到消息队列
        jmsMessagingTemplate.convertAndSend(queue, name);
//        方法二：这种方式不需要手动创建queue，系统会自行创建名为test的队列
//        jmsMessagingTemplate.convertAndSend("test", name);
    }

    //    订阅发布模式
    @RequestMapping(value = "/sendTopic")
    public String sendTopic(String message) {
//        指定消息发送的目的地和内容
        jmsMessagingTemplate.convertAndSend(topic, message);
        return "消息发送成功！message=" + message;
    }
}

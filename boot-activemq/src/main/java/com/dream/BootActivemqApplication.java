package com.dream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

@SpringBootApplication
// 允许启动ActiveMQ
@EnableJms
public class BootActivemqApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootActivemqApplication.class, args);
	}

}

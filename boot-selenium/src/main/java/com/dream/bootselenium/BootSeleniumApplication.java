package com.dream.bootselenium;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootSeleniumApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootSeleniumApplication.class, args);
	}

}

package com.dream.bootselenium.test;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

/**
 * 功能描述: <br>
 * 〈selenium自动化测试〉
 *
 * @Param:
 * @Return:
 * @Author: huajiancheng
 * @Date: 2020/9/12
 */


/**
 * 一个JUnit4的单元测试用例执行顺序为：
 *
 * @BeforeClass -> @Before -> @Test -> @After -> @AfterClass;
 * 每一个测试方法的调用顺序为：
 * @Before -> @Test -> @After;
 **/

@RunWith(SpringRunner.class)
@SpringBootTest
public class AutomatedBySeleniumTest {

    /**
     * CSDN官方地址
     **/
    private final static String CSDN_URL = "https://www.csdn.net/";

    /**
     * 谷歌驱动
     **/
    private static ChromeDriver browser;

    /**
     * 账号
     **/
    private final static String USERNAME = "*********";

    /**
     * 密码
     **/
    private final static String PASSWORD = "*********";

    /**
     * 打开谷歌浏览器
     **/
    @BeforeClass
    public static void openBrowser() {
        // 设置全局的驱动地址
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
        // 创建谷歌驱动对象
        browser = new ChromeDriver();
        // 浏览器实现自己模糊等待
        browser.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        browser.manage().window().maximize();
    }

    /**
     * 关闭谷歌浏览器
     **/
    @AfterClass
    public static void closeBrowser() {
        browser.quit();
    }


    /**
     * 功能描述: <br>
     * 〈寻找登录页面〉
     *
     * @Param: []
     * @Return: void
     * @Author: huajiancheng
     * @Date: 2020/9/12
     */
    public static String findLoginPage() {
        // 打开网址
        browser.get(CSDN_URL);
        // 找到登录注册进行点击跳转到真的登录页面
        WebElement webElement = browser.findElement(By.cssSelector("#csdn_container_tool > div > ul > li.userinfo > a"));
        if (webElement != null) {
            String realLoginUrl = webElement.getAttribute("href");
            // 模拟进行点击
            webElement.click();
            return realLoginUrl;
        }
        return "";
    }

    /**
     * 功能描述: <br>
     * 〈选择登录方式〉
     *
     * @Param: []
     * @Return: void
     * @Author: huajiancheng
     * @Date: 2020/9/12
     */
    public static void chooseLoginByWay(String loginUrl) throws InterruptedException {
        // 打开新的页面
        browser.get(loginUrl);
        // 找到账号密码登录，输入自己的账号和密码
        WebElement webElement = browser.findElement(By.cssSelector("#app > div > div > div.main > div.main-login > div.main-select > ul > li:nth-child(2) > a"));
        webElement.click();
        // 暂停3秒让页面先加载出来
        Thread.sleep(3000);
        // 输入账户和密码
        browser.findElement(By.id("all")).sendKeys(USERNAME);
        browser.findElement(By.id("password-number")).sendKeys(PASSWORD);

        // 点击登录 这里点击登录有两种方式

        // 第一种一般情况下有的网站登录在输入完成之后可以按回车键进行直接登录
        browser.findElement(By.id("password-number")).sendKeys(Keys.ENTER);

        // 第二种就是找到登录按钮进行模拟点击
        // 两个方法选择一个就可以了
        WebElement loginWebElement = browser.findElement(By.cssSelector("#app > div > div > div.main > div.main-login > div.main-process-login > div > div:nth-child(6) > div > button"));
        loginWebElement.click();
        // 点击登陆后有的时候会异常让你输入短信验证码什么的，只能手动输入
    }

    @Test
    public void start() throws InterruptedException {
        System.out.println("************************CSDN自动化测试启动方法************************");
        String realLoginUrl = findLoginPage();
        if (!realLoginUrl.equals("")) {
            Thread.sleep(2000);
            chooseLoginByWay(realLoginUrl);
        }
    }
}

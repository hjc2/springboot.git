package com.dream.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
// 开启 swagger2
@EnableSwagger2
public class SwaggerConfig {
    // 提供一个 Docket的bean
    @Bean
    public Docket createRestApi() {
        Docket docket = new Docket(DocumentationType.SWAGGER_2);
        docket.pathMapping("/").select().
                apis(RequestHandlerSelectors.basePackage("com.dream.controller")).
                paths(PathSelectors.any()).build().
                apiInfo(new ApiInfoBuilder().title("SpringBoot整合Swagger").
                        description("SpringBoot整合Swagger，详细信息......").
                        version("1.0").contact(new Contact("name", "blog.csdn.net", "aaa@gmail.com")).
                        license("The Apache License").licenseUrl("http://www.baidu.com").build());
        return docket;
    }
}

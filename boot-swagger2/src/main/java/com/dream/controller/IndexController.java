package com.dream.controller;

import com.dream.entity.BookEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/book")
// api是用来标记controller的功能
@Api(value = "测试相关的接口")
public class IndexController {
    // ApiOperation标记一个方法
    @ApiOperation(value = "首次访问接口")
    @RequestMapping(value = "/index",method = RequestMethod.GET)
    // 传入的参数
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "id", defaultValue = "1", required = true)})
    public String index(int id) {
        return "hello index\t" + id;
    }

    @ApiOperation(value = "书籍接口")
    @RequestMapping(value = "/book")
    @ApiImplicitParams({@ApiImplicitParam(name = "BookEntity")})
    public String hi(BookEntity bookEntity) {
        System.out.println(bookEntity);
        return bookEntity.toString();
    }
}

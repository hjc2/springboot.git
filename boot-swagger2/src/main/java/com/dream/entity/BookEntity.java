package com.dream.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

// 书籍实体
@ApiModel
public class BookEntity {
    @ApiModelProperty(value = "书籍的id编号")
    private String bookId;
    @ApiModelProperty(value = "书籍的名称")
    private String bookName;

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }
}
